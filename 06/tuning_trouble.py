with open('input.txt') as input_file:
    for line in input_file:
        line = line.strip('\n')
        
        last_four_items = []
        last_fourteen_items = []
        
        char_count = 0
        chars_to_packet_start = 0
        chars_to_message_start = 0
        
        for char in line:
            char_count += 1
            last_four_items.insert(0, char)
            last_fourteen_items.insert(0, char)
            if len(last_four_items) > 4:
                last_four_items.pop()
            if len(last_fourteen_items) > 14:
                last_fourteen_items.pop()
            
            if len(set(last_four_items)) == 4 and chars_to_packet_start == 0:
                chars_to_packet_start = char_count
            if len(set(last_fourteen_items)) == 14 and chars_to_message_start == 0:
                chars_to_message_start = char_count

print(f'Characters to find the first marker: {chars_to_packet_start}')
print(f'Characters to find the first message: {chars_to_message_start}')