tree_map = {}


def check_tree_vis(tree_list):
    prev_heights = [0]
    for tree in tree_list:
        tree_id = tree['tree_id']
        height = tree['height']

        visible = False
        if height > max(prev_heights) or tree == tree_list[-1]:
            visible = True

        if not tree_map[tree_id].get('visible') or tree_map[tree_id].get('visible') == False:
            tree_map[tree_id]['visible'] = visible
        prev_heights.append(height)


def calculate_scenic_score(tree, row, col):
    tree_height = tree['height']
    r_a = 0
    r_b = 0
    c_a = 0
    c_b = 0
    row_found = False
    col_found = False
    row_a = []
    row_b = []
    col_a = []
    col_b = []

    for t in row:
        if t == tree:
            row_found = True
            continue
        if not row_found:
            row_a.append(t)
        else:
            row_b.append(t)

    for t in col:
        if t == tree:
            col_found = True
            continue
        if not col_found:
            col_a.append(t)
        else:
            col_b.append(t)

    for t in row_a[::-1]:
        r_a += 1
        if t['height'] >= tree_height:
            break
    for t in row_b:
        r_b += 1
        if t['height'] >= tree_height:
            break
    for t in col_a[::-1]:
        c_a += 1
        if t['height'] >= tree_height:
            break
    for t in col_b:
        c_b += 1
        if t['height'] >= tree_height:
            break

    score = r_a * r_b * c_a * c_b
    return score


def main():
    cols = {}
    rows = {}
    tree_count = 0
    row = 0
    with open('input.txt') as input_file:
        lines = input_file.readlines()
        for line in lines:
            line = line.strip('\n')
            col = 0
            for char in line:
                tree_count += 1
                tree = {'height': int(char), 'tree_id': tree_count, 'row': row, 'col': col}
                tree_map[tree_count] = tree
                col += 1
            row += 1

    for tree_id in tree_map.keys():
        tree = tree_map[tree_id]
        tree_row = tree['row']
        tree_col = tree['col']

        if not rows.get(tree_row):
            rows[tree_row] = []
        rows[tree_row].append(tree)

        if not cols.get(tree_col):
            cols[tree_col] = []
        cols[tree_col].append(tree)

    for c in cols.keys():
        check_tree_vis(cols[c])
        check_tree_vis(cols[c][::-1])
    for r in rows.keys():
        check_tree_vis(rows[r])
        check_tree_vis(rows[r][::-1])

    vis_count = 0
    for tree in tree_map.keys():
        if tree_map[tree]['visible']:
            vis_count += 1
    print(f'There are this many visible trees: {vis_count}')

    high_score = 0
    for tree in tree_map.keys():
        row = tree_map[tree]['row']
        col = tree_map[tree]['col']
        score = calculate_scenic_score(tree_map[tree], rows[row], cols[col])
        if score > high_score:
            high_score = score
    print(f'The scenic high_score is: {high_score}')


main()
