import json

size_map = {}


def main():
    file_map = {}
    dir_path = []
    files = {}
    dirs = {'/': {'size': 0, 'parent_dir': 'none'}}
    list_mode = False
    file_size_sum = 0
    
    with open('input.txt') as input_file:
        for line in input_file:
            line = line.strip('\n')
            split_line = line.split(' ')
            
            if '$' and 'cd' in split_line:
                list_mode = False
                
                if '..' in split_line:
                    dir_path.pop()
                    print(dir_path)
                else:
                    dir_path.append(split_line[-1])
                    print(dir_path)

            
            if list_mode:
                if 'dir' in split_line:
                    dir_name = split_line[1]
                    parent_dir_path = dir_path
                    dir_path.append(dir_name)
                    adj_dir_path = dir_path.copy()
                    adj_dir_path.pop(0)
                    p_adj_dir_path = parent_dir_path.copy()
                    p_adj_dir_path.pop(0)
                    p_adj_dir_path.pop()
                    parent_dir = '/'.join(p_adj_dir_path)
                    if not parent_dir:
                        parent_dir = '/'
                    dirs['/'.join(adj_dir_path)] = {
                        'size': 0,
                        'parent_dir': parent_dir
                    }
                    dir_path.pop()
                if not 'dir' in split_line:
                    size = int(split_line[0])
                    file_name = split_line[1]
                    adj_dir_path = dir_path.copy()
                    adj_dir_path.pop(0)
                    dir_name = '/'.join(adj_dir_path)
                    if not dir_name:
                        dir_name = '/'
                    dir_path.append(file_name)
                    print(dir_path)
                    adj_dir_path = dir_path.copy()
                    adj_dir_path.pop(0)
                    files['/'.join(adj_dir_path)] = {
                        'size': size,
                        'parent_dir': dir_name 
                    }
                    dir_path.pop()
            
            if '$' and 'ls' in split_line:
                list_mode = True
    
    for file in files:
        # print(f'{file} {files[file]}')
        file_dir = files[file]['parent_dir']
        present_dir_size = dirs[file_dir].get('size', 0)
        new_size = present_dir_size + files[file]['size']
        dirs[file_dir]['size'] = new_size 
    
    longest_path = 0    
    # print(json.dumps(dirs, indent=4))
    for d in dirs.keys():
        if d == '/':
            path_len = 0
        else:
            path_len = len(d.split('/'))
            if path_len > longest_path:
                longest_path = path_len
    for x in range(longest_path+1, -1, -1):
        for d in dirs.keys():
            # print(d)
            # print(dirs[d])
            if d == '/':
                path_len = 0
            else:
                path_len = len(d.split('/'))
                
            if path_len == x:
                parent_dir = dirs[d].get('parent_dir')
                # print(f'parent_dir: {parent_dir}')
                if parent_dir != 'none':
                    parent_dir_current_size = dirs[parent_dir].get('size', 0)
                    current_dir_size = dirs[d]['size']
                    dirs[parent_dir]['size'] = parent_dir_current_size + current_dir_size

    dir_sum = 0
    # print(dirs)
    for d in dirs.keys():
        size = dirs[d].get('size')
        if size <= 100000:
            dir_sum += size
    print(f'the sum is: {dir_sum}')
    
    
    total_space = 70000000
    required_space = 30000000
    used_space = dirs['/']["size"]
    free_space = total_space - used_space
    target_size = required_space - free_space
    print(f'{free_space} available, freeing up {target_size}')
    
    smallest_size = total_space
    for d in dirs.keys():
        size = dirs[d]['size']
        if size >= target_size:
            if size < smallest_size:
                smallest_size = size
                target_dir = d
    print(f'Deleting {target_dir}: {smallest_size}')
    
main()