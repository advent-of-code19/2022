import json

size_map = {}


def main():
    file_map = generate_file_map()

    # walk_file_map(file_map)
    
    # size_sum = 0
    # for k in size_map.keys():
    #     if size_map[k] <= 100000:
    #         size_sum += size_map[k]
    
    # print(f'The size sum is: {size_sum}')

        
# def walk_file_map(d, prev=''):
#     dir_size = 0
#     for k, v in d.items():
#         if type(v) == dict:
#             size = walk_file_map(v, k)
#             dir_size += size
#         else:
#             dir_size += v
#     size_map[prev] = dir_size
#     return dir_size

            


def generate_file_map():
    file_map = {}
    dir_path = []
    files = []
    list_mode = False
    file_size_sum = 0
    
    with open('input.txt') as input_file:
        for line in input_file:
            line = line.strip('\n')
            
            split_line = line.split(' ')
            
            if '$' and 'cd' in split_line:
                list_mode = False
                
                if '..' in split_line:
                    dir_path.pop()
                    print(dir_path)
                else:
                    dir_path.append(split_line[-1])
                    print(dir_path)

            
            if list_mode:
                if 'dir' not in split_line:
                    size = int(split_line[0])
                    file_name = split_line[1]
                    dir_path.append(file_name)
                    adjusted_dir_path = dir_path
                    files.append('/'.join(dir_path))
                    dir_path.pop()
            
            if '$' and 'ls' in split_line:
                list_mode = True
    for file in files:
        print(file[1:])
    return file
    

# def nested_get(dic, keys):    
#     for key in keys:
#         dic = dic.get(key)
#     return dic
    
# def nested_set(dic, keys, value):
#     for key in keys[:-1]:
#         dic = dic.setdefault(key, {})
#     dic[keys[-1]] = value
    

main()