stacks = {
    1: ['D','H','N','Q','T','W','V','B'],
    2: ['D', 'W','B'],
    3: ['T','S','Q','W','J','C'],
    4: ['F','J','R','N','Z','T','P'],
    5: ['G','P','V','J','M','S','T'],
    6: ['B','W','F','T','N'],
    7: ['B','L','D','Q','F','H','V','N'],
    8: ['H','P','F','R'],
    9: ['Z','S','M','B','L','N','P','H']
}

with open('input.txt') as input_file:
    for line in input_file:
        line = line.strip('\n')
        if 'move' in line:
            split_line = line.split(' ')
            
            # clean string
            for i in split_line:
                if i in ['move', 'from', 'to']:
                    split_line.remove(i)
            
            amount = int(split_line[0])
            source = int(split_line[1])
            target = int(split_line[2])
            print(f'Moving {amount} from {source} to {target}')
            
            source_list = stacks[source]
            target_list = stacks[target]
            
            crates_to_move = []
            
            for x in range(amount):
                crates_to_move.append(source_list.pop())
            
            crates_to_move.reverse()

            for crate in crates_to_move:
                target_list.append(crate)
            
            stacks[source] = source_list
            stacks[target] = target_list
message = ''
for key in stacks.keys():
    message += stacks[key][-1]
print(f'the final stack message is: {message}')