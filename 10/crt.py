def part_1():
    cycle_sum = 0
    cycles_of_interest = [20, 60, 100, 140, 180, 220]
    current_cycle = 0
    x = 1

    with open('input.txt') as input_file:
        for line in input_file:
            split_line = line.strip('\n').split(' ')
            op = split_line[0]
            val = 0
            if op != 'noop':
                val = split_line[1]

            if op == 'noop':
                cycle_cost = 1
            else:
                cycle_cost = 2

            for i in range(cycle_cost):
                current_cycle += 1
                if current_cycle in cycles_of_interest:
                    signal_strength = x * current_cycle
                    cycle_sum += signal_strength
                if op == 'addx' and i == 1:
                    x += int(val)
    return cycle_sum


def part_2():
    current_cycle = 0
    x = 1
    row = ''
    with open('input.txt') as input_file:
        for line in input_file:
            split_line = line.strip('\n').split(' ')
            op = split_line[0]
            val = 0

            if op == 'noop':
                cycle_cost = 1
            else:
                cycle_cost = 2
                val = split_line[1]

            for i in range(cycle_cost):
                current_cycle += 1

                if op == 'addx' and i == 1:
                    x += int(val)

                delta = abs((current_cycle % 40) - x)
                if delta <= 1:
                    row += '#'
                else:
                    row += '.'

                if current_cycle % 40 == 0:
                    print(row)
                    row = ''


if __name__ == '__main__':
    print(part_1())
    part_2()
