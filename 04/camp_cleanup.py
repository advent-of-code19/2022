overlap_count = 0
intersect_count = 0
with open('input.txt') as input_file:
        for line in input_file:
            line = line.strip('\n')
            ranges = line.split(',')
            range_one = set(range(int(ranges[0].split('-')[0]), int(ranges[0].split('-')[1])+1))
            range_two = set(range(int(ranges[1].split('-')[0]), int(ranges[1].split('-')[1])+1))
            
            if range_one.issubset(range_two) or range_two.issubset(range_one):
                overlap_count += 1
            if range_one.intersection(range_two):
                intersect_count += 1
print(f'There are {overlap_count} overlapping ranges.')
print(f'There are {intersect_count} intersecting ranges.')