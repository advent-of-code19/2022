import json


def get_monkeys():
    monkeys = []
    print('Constructing monkeys...')
    with open('input.txt') as input_file:
        for line in input_file:
            line = line.strip('\n')
            if 'Monkey' in line:
                monkey = {
                    'inspections': 0,
                    'id': line.split(' ')[1].split(':')[0]
                }
            if 'Starting items:' in line:
                item_strings = line.split(': ')[1].split(', ')
                monkey['items'] = [int(i) for i in item_strings]
            if 'Operation:' in line:
                split_line = line.split(' ')
                monkey['operator'] = split_line[-2]
                monkey['operand'] = split_line[-1]
            if 'Test:' in line:
                monkey['test'] = int(line.split(' ')[-1])
            if 'If true:' in line:
                monkey['true_target'] = int(line.split(' ')[-1])
            if 'If false:' in line:
                monkey['false_target'] = int(line.split(' ')[-1])
                monkeys.append(monkey)

    monkey_count = 0
    for monkey in monkeys:
        monkey_count += 1
        print(f'{monkey_count} of {len(monkeys)}: {json.dumps(monkey)}')

    return monkeys


def worry(m, old, modulo=0):
    worry = 0
    operator = m.get('operator')
    operand = m.get('operand')

    if operand == 'old':
        operand = old
    else:
        operand = int(operand)

    if operator == '*':
        worry = old * operand
    elif operator == '/':
        worry = old / operand
    elif operator == '+':
        worry = old + operand
    elif operator == '-':
        worry = old - operand

    if modulo:
        worry %= modulo
    else:
        worry = worry // 3
    return worry


def monkey_test(worry, test):
    if worry % test == 0:
        return True
    return False


def part_one():
    monkeys = get_monkeys()
    print('Calculating monkey business...')
    for i in range(20):
        for monkey in monkeys:
            items_to_remove = []
            for index, item in enumerate(monkey['items']):
                worry_level = worry(monkey, item)

                test_results = monkey_test(worry_level, monkey['test'])

                if test_results:
                    target_monkey_index = monkey['true_target']
                else:
                    target_monkey_index = monkey['false_target']

                monkeys[target_monkey_index]['items'].append(worry_level)
                items_to_remove.append(item)
                monkey['inspections'] += 1

            for item in items_to_remove:
                monkey['items'].remove(item)

    inspections = []
    for monkey in monkeys:
        inspections.append(monkey.get('inspections'))

    print(inspections)

    busiest_monkeys = [inspections.index(x) for x in sorted(inspections, reverse=True)[:2]]
    print(busiest_monkeys)
    monkey_business = inspections[busiest_monkeys[0]] * inspections[busiest_monkeys[1]]

    print(f'{monkey_business} monkey business')


def part_two():
    monkeys = get_monkeys()
    modulo = 1
    for monkey in monkeys:
        modulo *= monkey['test']
    print(f'test divisor multiple: {modulo}')

    print('Calculating monkey business for part 2...')
    for i in range(10000):
        for monkey in monkeys:
            for item in monkey['items']:
                worry_level = worry(monkey, item, modulo)
                test_results = monkey_test(worry_level, monkey['test'])
                if test_results:
                    target_monkey_index = monkey['true_target']
                else:
                    target_monkey_index = monkey['false_target']
                monkeys[target_monkey_index]['items'].append(worry_level)
                monkey['inspections'] += 1
            monkey['items'] = []

    inspections = []
    for monkey in monkeys:
        inspections.append(monkey.get('inspections'))
    print(inspections)
    busiest_monkeys = [inspections.index(x) for x in sorted(inspections, reverse=True)[:2]]
    print(busiest_monkeys)
    monkey_business = inspections[busiest_monkeys[0]] * inspections[busiest_monkeys[1]]

    print(f'{monkey_business} monkey business')


if __name__ == '__main__':
    print('Monkey in the Middle\n---------------------')
    print('Part 1: ')
    part_one()
    print('Part 2: ')
    part_two()
