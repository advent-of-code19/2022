import numpy as np
import aocd


def encode_position(pos):
    return f'{pos["x"]} {pos["y"]}'


def part_1():
    tail_visits = set()
    tail_position = {'x': 0, 'y': 0}
    head_position = {'x': 0, 'y': 0}

    tail_visits.add(encode_position(tail_position))

    with open('input.txt') as input_file:
        for line in input_file:
            line = line.strip('\n')
            line_split = line.split(' ')
            direction = line_split[0]
            distance = int(line_split[1])

            for _ in range(distance):
                if direction == 'L':
                    head_position = {'x': head_position['x'] - 1, 'y': head_position['y']}
                if direction == 'R':
                    head_position = {'x': head_position['x'] + 1, 'y': head_position['y']}
                if direction == 'U':
                    head_position = {'x': head_position['x'], 'y': head_position['y'] + 1}
                if direction == 'D':
                    head_position = {'x': head_position['x'], 'y': head_position['y'] - 1}

                delta_x = head_position['x'] - tail_position['x']
                delta_y = head_position['y'] - tail_position['y']

                if abs(delta_x) > 1 or abs(delta_y) > 1:
                    tail_position = {
                        'x': (tail_position['x'] + np.sign(delta_x)),
                        'y': (tail_position['y'] + np.sign(delta_y))
                    }
                    tail_visits.add(encode_position(tail_position))

    print(f'The 2 knot tail has visited {len(tail_visits)} positions at least once.')


def part_2():
    with open('input.txt') as input_file:
        length = 10
        knots = []

        for k in range(length):
            knots.append((0, 0))

        tail_visits = set()
        tail_visits.add(knots[-1])

        for line in input_file:
            line = line.strip('\n')
            line_split = line.split(' ')
            direction = line_split[0]
            distance = int(line_split[1])

            for _ in range(distance):
                if direction == 'U':
                    knots[0] = (knots[0][0], knots[0][1] + 1)
                if direction == 'D':
                    knots[0] = (knots[0][0], knots[0][1] - 1)
                if direction == 'R':
                    knots[0] = (knots[0][0] + 1, knots[0][1])
                if direction == 'L':
                    knots[0] = (knots[0][0] - 1, knots[0][1])
                
                for k in range(length - 1):
                    delta_x = knots[k][0] - knots[k + 1][0]
                    delta_y = knots[k][1] - knots[k + 1][1]
                    if abs(delta_x) > 1 or abs(delta_y) > 1:
                        knots[k + 1] = (knots[k + 1][0] + np.sign(delta_x), knots[k + 1][1] + np.sign(delta_y))
                    tail_visits.add(knots[-1])
    print(f'The 10 knot tail has visited {len(tail_visits)} positions at least once.')


if __name__ == '__main__':
    part_1()
    part_2()
