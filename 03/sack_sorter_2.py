import string

letter_priority = {}

for index, letter in enumerate(string.ascii_lowercase + string.ascii_uppercase):
    letter_priority[letter] = index + 1

value_sum = 0
elf_group = []
with open('input.txt') as input_file:
    for line in input_file:
        line = line.strip('\n')
        elf_group.append(line)
        
        if len(elf_group) == 3:
            for key in letter_priority.keys():
                if key in elf_group[0] and key in elf_group[1] and key in elf_group[2]:
                    print(key)
                    print(elf_group)
                    value_sum += letter_priority[key]
            elf_group = []
        
print(value_sum)
