import string

letter_priority = {}

for index, letter in enumerate(string.ascii_lowercase + string.ascii_uppercase):
    letter_priority[letter] = index + 1

value_sum = 0

with open('input.txt') as input_file:
        for line in input_file:
            line = line.strip('\n')
            sack_one, sack_two = line[:len(line)//2], line[len(line)//2:]
            for key in letter_priority.keys():
                if key in sack_one and key in sack_two:
                    value_sum += letter_priority[key]
print(value_sum)
