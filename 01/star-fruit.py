elf_list = []
with open('input.txt') as input:
    elf_buffer = []
    
    for line in input:
        if line != '\n':
            line.strip('\n')
            elf_buffer.append(line)
        else: 
            elf_list.append(elf_buffer)
            elf_buffer = []

print(f'{len(elf_list)} elves found')
print('finding thickest elves...')

elf_sums = []
for elf in elf_list:
    calories = 0
    for fruit in elf:
        calories += int(fruit)
    elf_sums.append(calories)
    
    
final_list = []
 
for i in range(0, 3):
    max1 = 0
     
    for j in range(len(elf_sums)):    
        if elf_sums[j] > max1:
            max1 = elf_sums[j]
             
    elf_sums.remove(max1)
    final_list.append(max1)
     
print(f'the three thickest elves are {final_list}')

elf_sum = 0
for elf in final_list:
    elf_sum += elf


print(f'The 3 thickest eves have {elf_sum} calories.')