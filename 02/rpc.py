PART = 2

def main():
    point_sum = 0
    
    print(f'Playing rock paper scissors...')
    
    with open('input.txt') as input_file:
        for line in input_file:
            line = line.strip('\n')
            line_split = line.split(' ')
            opponent = decode(line_split[0])
            me = decode(line_split[-1])
            
            points = get_result(opponent, me)
            print(f'opponent: {opponent}, me: {me}, points: {points}')
            point_sum += points
    
    print(f'You have accumulated {point_sum} points!')

def get_result(o, m):
    points = {
        'win': 6,
        'draw': 3,
        'lose': 0
    }
    
    game_data = {
        'rock': {
            'scissors': 'win',
            'paper': 'lose',
            'value': 1
        },
        'paper': {
            'rock': 'win',
            'scissors': 'lose',
            'value': 2
        },
        'scissors': {
            'paper': 'win',
            'rock': 'lose',
            'value': 3
        },
    }
    
    if PART == 1:
        if o == m:
            result = 'draw'
            return ((game_data[o]['value']) + points[result])
        else:
           return points[game_data[m][o]] + game_data[m]['value']
    
    if PART == 2:
        if m == 'draw':
            return ((game_data[o]['value']) + points[m])
        else:
            for key in game_data[o].keys():
                if game_data[key][o] == m:
                    return points[game_data[key][o]] + game_data[key]['value']
        
        
def decode(code):
    if PART == 1:
        decoder = {
            'A': 'rock',
            'B': 'paper',
            'C': 'scissors',
            'X': 'rock',
            'Y': 'paper',
            'Z': 'scissors'
        }
        
    if PART == 2:
        decoder = {
        'A': 'rock',
        'B': 'paper',
        'C': 'scissors',
        'X': 'lose',
        'Y': 'draw',
        'Z': 'win'
    }
    
    return decoder[code]


main()